﻿using System;
using Gtk;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;

/// <summary>
/// Main window of ADB ToolKit.
/// ADB ToolKit is a GUI-based software for controlling an Android TV from PC.
/// Created by Nurul Aiman, @mankidal19 on 2018.
/// For more source codes, visit https://bitbucket.org/mankidal19/
/// </summary>
public partial class MainWindow : Gtk.Window
{
    private ProcessStartInfo startInfo;

    public MainWindow() : base(Gtk.WindowType.Toplevel)
    {
        Build();

        startInfo = new ProcessStartInfo { 
        FileName = "adb",
        UseShellExecute = false,
        RedirectStandardOutput = true,
        CreateNoWindow = true
        };
    }



    protected void OnDeleteEvent(object sender, DeleteEventArgs a)
    {
        Gtk.Application.Quit();
        a.RetVal = true;
    }

    /// <summary>
    /// On the reboot button clicked.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnRebootButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb reboot");
        CreateNewProcess("reboot");
        MessageBox.Show("Device is Now Rebooting..");


    }

    /// <summary>
    /// On the start server button clicked.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnStartServerButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb start-server");
        CreateNewProcess("start-server");

        MessageBox.Show("Starting ADB server..");
    }

    /// <summary>
    /// On the kill server button clicked.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnKillServerButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb kill-server");
        CreateNewProcess("kill-server");

        MessageBox.Show("Killing server..");

    }

    /// <summary>
    /// On the red button clicked.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnRedButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb shell input keyevent KEYCODE_PROG_RED");
        CreateNewProcess("shell input keyevent KEYCODE_PROG_RED");

    }

    /// <summary>
    /// On the green button clicked.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnGreenButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb shell input keyevent KEYCODE_PROG_GREEN");
        CreateNewProcess("shell input keyevent KEYCODE_PROG_GREEN");

    }

    /// <summary>
    /// On the yellow button clicked.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnYellowButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb shell input keyevent KEYCODE_PROG_YELLOW");
        CreateNewProcess("shell input keyevent KEYCODE_PROG_YELLOW");

    }

    /// <summary>
    /// On the blue button clicked.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnBlueButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb shell input keyevent KEYCODE_PROG_BLUE");
        CreateNewProcess("shell input keyevent KEYCODE_PROG_BLUE");

    }

    /// <summary>
    /// On the volume up button clicked.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnVolumeUpButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb shell input keyevent KEYCODE_VOLUME_UP");
        CreateNewProcess("shell input keyevent KEYCODE_VOLUME_UP");

    }

    /// <summary>
    /// On the volume down button clicked.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnVolumeDownButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb shell input keyevent KEYCODE_VOLUME_DOWN");
        CreateNewProcess("shell input keyevent KEYCODE_VOLUME_DOWN");

    }

    /// <summary>
    /// On the channel up button clicked.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnChannelUpButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb shell input keyevent KEYCODE_CHANNEL_UP");
        CreateNewProcess("shell input keyevent KEYCODE_CHANNEL_UP");

    }

    /// <summary>
    /// On the channel down button clicked.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnChannelDownButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb shell input keyevent KEYCODE_CHANNEL_DOWN");
        CreateNewProcess("shell input keyevent KEYCODE_CHANNEL_DOWN");

    }

    /// <summary>
    /// On the back button clicked.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnBackButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb shell input keyevent KEYCODE_BACK");
        CreateNewProcess("shell input keyevent KEYCODE_BACK");

    }

    /// <summary>
    /// On the home button clicked.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnHomeButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb shell input keyevent KEYCODE_HOME");
        CreateNewProcess("shell input keyevent KEYCODE_HOME");

    }

    /// <summary>
    /// On the D-pad center button clicked.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnOkButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb shell input keyevent KEYCODE_DPAD_CENTER");
        CreateNewProcess("shell input keyevent KEYCODE_DPAD_CENTER");

    }

    /// <summary>
    /// On D-pad up button clicked.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnUpButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb shell input keyevent KEYCODE_DPAD_UP");
        CreateNewProcess("shell input keyevent KEYCODE_DPAD_UP");

    }

    /// <summary>
    /// On D-pad down button clicked.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnDownButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb shell input keyevent KEYCODE_DPAD_DOWN");
        CreateNewProcess("shell input keyevent KEYCODE_DPAD_DOWN");

    }

    /// <summary>
    /// On the D-pad left button clicked.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnLeftButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb shell input keyevent KEYCODE_DPAD_LEFT");
        CreateNewProcess("shell input keyevent KEYCODE_DPAD_LEFT");

    }

    /// <summary>
    /// On the D-pad right button clicked.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnRightButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb shell input keyevent KEYCODE_DPAD_RIGHT");
        CreateNewProcess("shell input keyevent KEYCODE_DPAD_RIGHT");

    }

    /// <summary>
    /// On the play button clicked.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnPlayButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb shell input keyevent KEYCODE_MEDIA_PLAY");
        CreateNewProcess("shell input keyevent KEYCODE_MEDIA_PLAY");

    }

    /// <summary>
    /// On the pause button clicked.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnPauseButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb shell input keyevent KEYCODE_MEDIA_PAUSE");
        CreateNewProcess("shell input keyevent KEYCODE_MEDIA_PAUSE");

    }

    /// <summary>
    /// On the stop button clicked.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnStopButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb shell input keyevent KEYCODE_MEDIA_STOP");
        CreateNewProcess("shell input keyevent KEYCODE_MEDIA_STOP");

    }

    /// <summary>
    /// On the rewind button clicked.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnRewindButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb shell input keyevent KEYCODE_MEDIA_REWIND");
        CreateNewProcess("shell input keyevent KEYCODE_MEDIA_REWIND");

    }

    /// <summary>
    /// On the fastforward button clicked.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnFastforwardButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb shell input keyevent KEYCODE_MEDIA_FAST_FORWARD");
        CreateNewProcess("shell input keyevent KEYCODE_MEDIA_FAST_FORWARD");

    }

    /// <summary>
    /// On the skip previous button clicked.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnSkipPrevButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb shell input keyevent KEYCODE_MEDIA_PREVIOUS");
        CreateNewProcess("shell input keyevent KEYCODE_MEDIA_PREVIOUS");

    }

    /// <summary>
    /// On the skip next button clicked.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnSkipNextButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb shell input keyevent KEYCODE_MEDIA_NEXT");
        CreateNewProcess("shell input keyevent KEYCODE_MEDIA_NEXT");

    }

    /// <summary>
    /// On the browse button clicked.
    /// To browse PC's directory in finding the desired APK file.
    /// <seealso cref="MainWindow.CreateFileChooser(string, FileChooserAction, string, string, string, string, TextView, FileFilter)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnBrowseButtonClicked(object sender, EventArgs e)
    {
        //Create a filter object & add APK file's pattern
        FileFilter filter = new FileFilter();
        filter.Name = "APK files";
        filter.AddPattern("*.APK");
        filter.AddPattern("*.apk");



        CreateFileChooser("Select an APK file", FileChooserAction.Open, "Select",
        "browse for an APK file", "an APK file selected", "cancel browse", 
            ApkFilePathText, filter);


    }

    /// <summary>
    /// On the install button clicked.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnInstallButtonClicked(object sender, EventArgs e)
    {
        if (ApkFilePathText.Buffer.Text.Contains(".apk")) {
            UpdateLog("adb install -r " + ApkFilePathText.Buffer.Text);
            CreateNewProcess("install -r " + ApkFilePathText.Buffer.Text);

            MessageBox.Show("APK file is successfully installed", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        else {
            UpdateLog("Invalid APK file.");
            MessageBox.Show("adb install unsuccessful due to invalid APK file.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }



    }

    /// <summary>
    /// On the execute button clicked.
    /// To execute custom adb command.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnExecuteButtonClicked(object sender, EventArgs e)
    {
        if (customCommandText.Buffer.Text.Contains("adb")) {

            string commandStr = customCommandText.Buffer.Text.Substring(4);
            UpdateLog("adb " + commandStr);
            CreateNewProcess(commandStr);
        }

        else {
            UpdateLog("invalid adb command.");
            MessageBox.Show("Invalid adb command.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);}


    }

    /// <summary>
    /// On the connect button clicked.
    /// To connect with an Android TV device using its IP address.
    /// <seealso cref="MainWindow.UpdateLog(string)"/>
    /// <seealso cref="MainWindow.CreateNewProcess(string)"/>
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnConnectButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb connect " + adbIPAddressInput.Buffer.Text);
        CreateNewProcess("connect " + adbIPAddressInput.Buffer.Text);

    }

    /// <summary>
    /// On the kill button clicked.
    /// To kill the specified application in the packageName textview.
    /// </summary>
    /// <param name="sender">Sender object.</param>
    /// <param name="e">EventArgs object.</param>
    protected void OnKillButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb shell am kill " + packageName.Buffer.Text);
        CreateNewProcess("shell am kill " + packageName.Buffer.Text);
    }

    /// <summary>
    /// On the devices button clicked.
    /// To get the list of Android devices currently connected.
    /// </summary>
    /// <param name="sender">Sender.</param>
    /// <param name="e">E.</param>
    protected void OnDevicesButtonClicked(object sender, EventArgs e)
    {
        UpdateLog("adb devices");
        CreateNewProcess("devices");

    }

    /// <summary>
    /// On the screencap button clicked.
    /// To capture the current Android TV screen and save it locally to the PC at the desired directory.
    /// <seealso cref="MainWindow.CreateFileChooser(string, FileChooserAction, string, string, string, string, TextView, FileFilter)(string)"/>
    /// </summary>
    /// <param name="sender">Sender.</param>
    /// <param name="e">E.</param>
    protected void OnScreencapButtonClicked(object sender, EventArgs e)
    {
        //Create a filter object & add APK file's pattern
        FileFilter filter = new FileFilter();
        filter.Name = "APK files";
        filter.AddPattern("*.png");
        filter.AddPattern("*.PNG");

        CreateFileChooser("Save screencap image", FileChooserAction.Save, "Save",
        "saving the screencap image", "file path selected", "cancel browse", screencapDirectoryText, filter);

        if (screencapDirectoryText.Buffer.Text.Contains(".png"))
        {
            UpdateLog("adb shell screencap -p /sdcard/screencap.png");
            //take a screencap & save locally on Android TV
            CreateNewProcess("shell screencap -p /sdcard/screencap.png");

            UpdateLog("adb pull /sdcard/screencap.png " + screencapDirectoryText.Buffer.Text);
            //pull the screencap & save to desired directory
            CreateNewProcess("pull /sdcard/screencap.png " + screencapDirectoryText.Buffer.Text);

            MessageBox.Show("Screencap successfully saved at " + screencapDirectoryText.Buffer.Text, "", 
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        else
        {
            UpdateLog("Invalid png file.");
            MessageBox.Show("adb install unsuccessful due to invalid png file.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

    }

    /// <summary>
    /// Creates a new file chooser dialog.
    /// /// <para><see cref="Gtk.FileChooserDialog"> for information regarding GTK's File Chooser Dialog.</see></para>
    /// </summary>
    /// <param name="chooserTitle">FileChooserDialog title.</param>
    /// <param name="action">FileChooserDialog Action.</param>
    /// <param name="acceptResponse">Accept response button string.</param>
    /// <param name="initialLogMsg">Initial log message.</param>
    /// <param name="acceptLogMsg">Accept log message.</param>
    /// <param name="cancelLogMsg">Cancel log message.</param>
    /// <param name="updateTextView">Textview to be updated.</param>
    /// <param name="filter">Filter (can be null).</param>
    private void CreateFileChooser(string chooserTitle, FileChooserAction action, string acceptResponse, string initialLogMsg, string acceptLogMsg, string cancelLogMsg, TextView updateTextView, FileFilter filter) {

        //create new FileChooserDialog object
        Gtk.FileChooserDialog filechooser =
        new Gtk.FileChooserDialog(chooserTitle,
            this,
            action,
            "Cancel", ResponseType.Cancel,
            acceptResponse, ResponseType.Accept);

        UpdateLog(initialLogMsg);

        if (filter != null) {
            filechooser.Filter = filter;
        }

        if (action.Equals(FileChooserAction.Save))
        {
            filechooser.CurrentName = "screencap.png";
            //if file already exists, do an overwrite confirmation
            filechooser.DoOverwriteConfirmation = true;
        }

        //if a file/folder is selected, append the textview with chosen file's path
        if (filechooser.Run() == (int)ResponseType.Accept)
        {

            updateTextView.Buffer.Text = filechooser.Filename;

            UpdateLog(acceptLogMsg);
        }

        //else update log message
        else
        {
            UpdateLog(cancelLogMsg);
        }

        filechooser.Destroy();

    }

    /// <summary>
    /// Updates the log message output by passing the desired message string.
    /// </summary>
    /// <param name="message">A string of message.</param>
    private void UpdateLog(string message) {

        //Create log message for the message passed
        string datetime = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
        message = datetime + ":: " + "execute ADB command: " + message;

        //get current log message output
        string currText = logText.Buffer.Text;

        //combine the new message with current one
        string newText = currText + "\n" + message;

        //append the log text output
        logText.Buffer.Text = newText;

        //to autoscroll to the bottom of the textview
        logText.Buffer.CreateMark("end", logText.Buffer.EndIter, false);
        TextMark textMark = logText.Buffer.GetMark("end");
        logText.ScrollToMark(textMark, 0, false, 0, 0);

    
    }

    /// <summary>
    /// Updates the log message output by passing the desired process.
    /// </summary>
    /// <param name="process">Process.</param>
    private void UpdateLog(Process process)
    {
        //while the current process produces a new message
        while (!process.StandardOutput.EndOfStream)
        {
            //Create log message for the new process
            string message = process.StandardOutput.ReadLine();
            string datetime = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
            message = datetime + ": " + message;

            //get current log message output
            string currText = logText.Buffer.Text;

            //combine the new message with current one
            string newText = currText + "\n" + message;

            //append the log text output
            logText.Buffer.Text = newText;

            //to autoscroll to the bottom of the textview
            logText.Buffer.CreateMark("end", logText.Buffer.EndIter, false);
            TextMark textMark = logText.Buffer.GetMark("end");
            logText.ScrollToMark(textMark, 0, false, 0, 0);


        }
    }


    /// <summary>
    /// Creates a new process and execute it.
    /// <seealso cref="MainWindow.UpdateLog(Process)"/>
    /// </summary>
    /// <param name="args">Arguments.</param>
    private void CreateNewProcess(string args) {

        var process = new Process();
        process.StartInfo = startInfo;
        process.StartInfo.Arguments = args;

        process.Start();

        UpdateLog(process);

        process.WaitForExit();
    }

   
}
