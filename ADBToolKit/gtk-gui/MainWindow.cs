
// This file has been generated by the GUI designer. Do not modify.

public partial class MainWindow
{
	private global::Gtk.VBox vbox3;

	private global::Gtk.HBox hbox4;

	private global::Gtk.Button startServerButton;

	private global::Gtk.Button killServerButton;

	private global::Gtk.Button devicesButton;

	private global::Gtk.Button rebootButton;

	private global::Gtk.HBox hbox11;

	private global::Gtk.Label label6;

	private global::Gtk.TextView screencapDirectoryText;

	private global::Gtk.Button screencapButton;

	private global::Gtk.HBox hbox1;

	private global::Gtk.Label label1;

	private global::Gtk.TextView ApkFilePathText;

	private global::Gtk.VBox vbox6;

	private global::Gtk.VBox vbox7;

	private global::Gtk.HBox hbox2;

	private global::Gtk.Button browseButton;

	private global::Gtk.Button installButton;

	private global::Gtk.HBox hbox3;

	private global::Gtk.Label label2;

	private global::Gtk.TextView adbIPAddressInput;

	private global::Gtk.Button connectButton;

	private global::Gtk.HBox hbox10;

	private global::Gtk.Label label5;

	private global::Gtk.TextView packageName;

	private global::Gtk.Button kill_button;

	private global::Gtk.HBox hbox5;

	private global::Gtk.Button redButton;

	private global::Gtk.Button greenButton;

	private global::Gtk.Button yellowButton;

	private global::Gtk.Button blueButton;

	private global::Gtk.HBox hbox6;

	private global::Gtk.VBox vbox11;

	private global::Gtk.Button volumeUpButton;

	private global::Gtk.Button volumeDownButton;

	private global::Gtk.Button backButton;

	private global::Gtk.VBox vbox9;

	private global::Gtk.Button channelUpButton;

	private global::Gtk.Button channelDownButton;

	private global::Gtk.Button homeButton;

	private global::Gtk.Button leftButton;

	private global::Gtk.VBox vbox10;

	private global::Gtk.Button upButton;

	private global::Gtk.Button okButton;

	private global::Gtk.Button downButton;

	private global::Gtk.Button rightButton;

	private global::Gtk.HBox hbox7;

	private global::Gtk.Button rewindButton;

	private global::Gtk.Button playButton;

	private global::Gtk.Button fastforwardButton;

	private global::Gtk.HBox hbox8;

	private global::Gtk.Button skipPrevButton;

	private global::Gtk.Button pauseButton;

	private global::Gtk.Button skipNextButton;

	private global::Gtk.Button stopButton;

	private global::Gtk.HBox hbox9;

	private global::Gtk.Label label4;

	private global::Gtk.TextView customCommandText;

	private global::Gtk.Button executeButton;

	private global::Gtk.Label label3;

	private global::Gtk.ScrolledWindow GtkScrolledWindow;

	private global::Gtk.TextView logText;

	protected virtual void Build()
	{
		global::Stetic.Gui.Initialize(this);
		// Widget MainWindow
		this.WidthRequest = 550;
		this.Name = "MainWindow";
		this.Title = global::Mono.Unix.Catalog.GetString("mankidal19\'s ADB Toolkit");
		this.WindowPosition = ((global::Gtk.WindowPosition)(4));
		this.Resizable = false;
		// Container child MainWindow.Gtk.Container+ContainerChild
		this.vbox3 = new global::Gtk.VBox();
		this.vbox3.Name = "vbox3";
		this.vbox3.Spacing = 6;
		// Container child vbox3.Gtk.Box+BoxChild
		this.hbox4 = new global::Gtk.HBox();
		this.hbox4.Name = "hbox4";
		this.hbox4.Homogeneous = true;
		this.hbox4.Spacing = 6;
		this.hbox4.BorderWidth = ((uint)(6));
		// Container child hbox4.Gtk.Box+BoxChild
		this.startServerButton = new global::Gtk.Button();
		this.startServerButton.CanFocus = true;
		this.startServerButton.Name = "startServerButton";
		this.startServerButton.UseUnderline = true;
		this.startServerButton.Label = global::Mono.Unix.Catalog.GetString("ADB Start Server");
		this.hbox4.Add(this.startServerButton);
		global::Gtk.Box.BoxChild w1 = ((global::Gtk.Box.BoxChild)(this.hbox4[this.startServerButton]));
		w1.Position = 0;
		w1.Expand = false;
		w1.Fill = false;
		// Container child hbox4.Gtk.Box+BoxChild
		this.killServerButton = new global::Gtk.Button();
		this.killServerButton.CanFocus = true;
		this.killServerButton.Name = "killServerButton";
		this.killServerButton.UseUnderline = true;
		this.killServerButton.Label = global::Mono.Unix.Catalog.GetString("ADB Kill Server");
		this.hbox4.Add(this.killServerButton);
		global::Gtk.Box.BoxChild w2 = ((global::Gtk.Box.BoxChild)(this.hbox4[this.killServerButton]));
		w2.Position = 1;
		w2.Expand = false;
		w2.Fill = false;
		// Container child hbox4.Gtk.Box+BoxChild
		this.devicesButton = new global::Gtk.Button();
		this.devicesButton.CanFocus = true;
		this.devicesButton.Name = "devicesButton";
		this.devicesButton.UseUnderline = true;
		this.devicesButton.Label = global::Mono.Unix.Catalog.GetString("ADB Devices");
		this.hbox4.Add(this.devicesButton);
		global::Gtk.Box.BoxChild w3 = ((global::Gtk.Box.BoxChild)(this.hbox4[this.devicesButton]));
		w3.PackType = ((global::Gtk.PackType)(1));
		w3.Position = 2;
		w3.Expand = false;
		w3.Fill = false;
		this.vbox3.Add(this.hbox4);
		global::Gtk.Box.BoxChild w4 = ((global::Gtk.Box.BoxChild)(this.vbox3[this.hbox4]));
		w4.Position = 0;
		w4.Expand = false;
		w4.Fill = false;
		// Container child vbox3.Gtk.Box+BoxChild
		this.rebootButton = new global::Gtk.Button();
		this.rebootButton.CanFocus = true;
		this.rebootButton.Name = "rebootButton";
		this.rebootButton.UseUnderline = true;
		this.rebootButton.Label = global::Mono.Unix.Catalog.GetString("REBOOT");
		this.vbox3.Add(this.rebootButton);
		global::Gtk.Box.BoxChild w5 = ((global::Gtk.Box.BoxChild)(this.vbox3[this.rebootButton]));
		w5.Position = 1;
		w5.Expand = false;
		w5.Fill = false;
		// Container child vbox3.Gtk.Box+BoxChild
		this.hbox11 = new global::Gtk.HBox();
		this.hbox11.Name = "hbox11";
		this.hbox11.Spacing = 6;
		// Container child hbox11.Gtk.Box+BoxChild
		this.label6 = new global::Gtk.Label();
		this.label6.Name = "label6";
		this.label6.LabelProp = global::Mono.Unix.Catalog.GetString("Screenshot:");
		this.hbox11.Add(this.label6);
		global::Gtk.Box.BoxChild w6 = ((global::Gtk.Box.BoxChild)(this.hbox11[this.label6]));
		w6.Position = 0;
		w6.Expand = false;
		w6.Fill = false;
		w6.Padding = ((uint)(4));
		// Container child hbox11.Gtk.Box+BoxChild
		this.screencapDirectoryText = new global::Gtk.TextView();
		this.screencapDirectoryText.Buffer.Text = global::Mono.Unix.Catalog.GetString("Select a folder directory");
		this.screencapDirectoryText.Name = "screencapDirectoryText";
		this.screencapDirectoryText.Editable = false;
		this.screencapDirectoryText.AcceptsTab = false;
		this.hbox11.Add(this.screencapDirectoryText);
		global::Gtk.Box.BoxChild w7 = ((global::Gtk.Box.BoxChild)(this.hbox11[this.screencapDirectoryText]));
		w7.Position = 1;
		w7.Padding = ((uint)(6));
		// Container child hbox11.Gtk.Box+BoxChild
		this.screencapButton = new global::Gtk.Button();
		this.screencapButton.CanFocus = true;
		this.screencapButton.Name = "screencapButton";
		this.screencapButton.UseUnderline = true;
		this.screencapButton.Label = global::Mono.Unix.Catalog.GetString("Capture");
		this.hbox11.Add(this.screencapButton);
		global::Gtk.Box.BoxChild w8 = ((global::Gtk.Box.BoxChild)(this.hbox11[this.screencapButton]));
		w8.Position = 2;
		w8.Expand = false;
		w8.Fill = false;
		this.vbox3.Add(this.hbox11);
		global::Gtk.Box.BoxChild w9 = ((global::Gtk.Box.BoxChild)(this.vbox3[this.hbox11]));
		w9.Position = 2;
		w9.Expand = false;
		w9.Fill = false;
		// Container child vbox3.Gtk.Box+BoxChild
		this.hbox1 = new global::Gtk.HBox();
		this.hbox1.Name = "hbox1";
		this.hbox1.Spacing = 6;
		// Container child hbox1.Gtk.Box+BoxChild
		this.label1 = new global::Gtk.Label();
		this.label1.Name = "label1";
		this.label1.LabelProp = global::Mono.Unix.Catalog.GetString("Browse APK:");
		this.hbox1.Add(this.label1);
		global::Gtk.Box.BoxChild w10 = ((global::Gtk.Box.BoxChild)(this.hbox1[this.label1]));
		w10.Position = 0;
		w10.Expand = false;
		w10.Fill = false;
		w10.Padding = ((uint)(4));
		// Container child hbox1.Gtk.Box+BoxChild
		this.ApkFilePathText = new global::Gtk.TextView();
		this.ApkFilePathText.CanFocus = true;
		this.ApkFilePathText.Name = "ApkFilePathText";
		this.ApkFilePathText.Editable = false;
		this.hbox1.Add(this.ApkFilePathText);
		global::Gtk.Box.BoxChild w11 = ((global::Gtk.Box.BoxChild)(this.hbox1[this.ApkFilePathText]));
		w11.Position = 1;
		// Container child hbox1.Gtk.Box+BoxChild
		this.vbox6 = new global::Gtk.VBox();
		this.vbox6.Name = "vbox6";
		this.vbox6.Spacing = 6;
		// Container child vbox6.Gtk.Box+BoxChild
		this.vbox7 = new global::Gtk.VBox();
		this.vbox7.Name = "vbox7";
		this.vbox7.Spacing = 6;
		// Container child vbox7.Gtk.Box+BoxChild
		this.hbox2 = new global::Gtk.HBox();
		this.hbox2.Spacing = 6;
		// Container child hbox2.Gtk.Box+BoxChild
		this.browseButton = new global::Gtk.Button();
		this.browseButton.CanFocus = true;
		this.browseButton.Name = "browseButton";
		this.browseButton.UseUnderline = true;
		this.browseButton.Label = global::Mono.Unix.Catalog.GetString("Browse");
		this.hbox2.Add(this.browseButton);
		global::Gtk.Box.BoxChild w12 = ((global::Gtk.Box.BoxChild)(this.hbox2[this.browseButton]));
		w12.Position = 0;
		w12.Expand = false;
		w12.Fill = false;
		// Container child hbox2.Gtk.Box+BoxChild
		this.installButton = new global::Gtk.Button();
		this.installButton.CanFocus = true;
		this.installButton.Name = "installButton";
		this.installButton.UseUnderline = true;
		this.installButton.Label = global::Mono.Unix.Catalog.GetString("Install");
		this.hbox2.Add(this.installButton);
		global::Gtk.Box.BoxChild w13 = ((global::Gtk.Box.BoxChild)(this.hbox2[this.installButton]));
		w13.Position = 1;
		w13.Expand = false;
		w13.Fill = false;
		this.vbox7.Add(this.hbox2);
		global::Gtk.Box.BoxChild w14 = ((global::Gtk.Box.BoxChild)(this.vbox7[this.hbox2]));
		w14.Position = 0;
		w14.Expand = false;
		w14.Fill = false;
		this.vbox6.Add(this.vbox7);
		global::Gtk.Box.BoxChild w15 = ((global::Gtk.Box.BoxChild)(this.vbox6[this.vbox7]));
		w15.Position = 0;
		w15.Expand = false;
		w15.Fill = false;
		this.hbox1.Add(this.vbox6);
		global::Gtk.Box.BoxChild w16 = ((global::Gtk.Box.BoxChild)(this.hbox1[this.vbox6]));
		w16.Position = 2;
		w16.Expand = false;
		w16.Fill = false;
		this.vbox3.Add(this.hbox1);
		global::Gtk.Box.BoxChild w17 = ((global::Gtk.Box.BoxChild)(this.vbox3[this.hbox1]));
		w17.Position = 3;
		w17.Expand = false;
		w17.Fill = false;
		// Container child vbox3.Gtk.Box+BoxChild
		this.hbox3 = new global::Gtk.HBox();
		this.hbox3.Name = "hbox3";
		this.hbox3.Spacing = 6;
		this.hbox3.BorderWidth = ((uint)(6));
		// Container child hbox3.Gtk.Box+BoxChild
		this.label2 = new global::Gtk.Label();
		this.label2.Name = "label2";
		this.label2.Xalign = 0F;
		this.label2.LabelProp = global::Mono.Unix.Catalog.GetString("Connect ADB (enter IP address):");
		this.hbox3.Add(this.label2);
		global::Gtk.Box.BoxChild w18 = ((global::Gtk.Box.BoxChild)(this.hbox3[this.label2]));
		w18.Position = 0;
		w18.Expand = false;
		w18.Fill = false;
		// Container child hbox3.Gtk.Box+BoxChild
		this.adbIPAddressInput = new global::Gtk.TextView();
		this.adbIPAddressInput.Buffer.Text = "172.16.101.3";
		this.adbIPAddressInput.CanFocus = true;
		this.adbIPAddressInput.Name = "adbIPAddressInput";
		this.hbox3.Add(this.adbIPAddressInput);
		global::Gtk.Box.BoxChild w19 = ((global::Gtk.Box.BoxChild)(this.hbox3[this.adbIPAddressInput]));
		w19.Position = 1;
		// Container child hbox3.Gtk.Box+BoxChild
		this.connectButton = new global::Gtk.Button();
		this.connectButton.CanFocus = true;
		this.connectButton.Name = "connectButton";
		this.connectButton.UseUnderline = true;
		this.connectButton.Label = global::Mono.Unix.Catalog.GetString("Connect");
		this.hbox3.Add(this.connectButton);
		global::Gtk.Box.BoxChild w20 = ((global::Gtk.Box.BoxChild)(this.hbox3[this.connectButton]));
		w20.Position = 2;
		w20.Expand = false;
		w20.Fill = false;
		this.vbox3.Add(this.hbox3);
		global::Gtk.Box.BoxChild w21 = ((global::Gtk.Box.BoxChild)(this.vbox3[this.hbox3]));
		w21.Position = 4;
		w21.Expand = false;
		w21.Fill = false;
		// Container child vbox3.Gtk.Box+BoxChild
		this.hbox10 = new global::Gtk.HBox();
		this.hbox10.Name = "hbox10";
		this.hbox10.Spacing = 6;
		// Container child hbox10.Gtk.Box+BoxChild
		this.label5 = new global::Gtk.Label();
		this.label5.Name = "label5";
		this.label5.LabelProp = global::Mono.Unix.Catalog.GetString("Kill App (enter package name):");
		this.hbox10.Add(this.label5);
		global::Gtk.Box.BoxChild w22 = ((global::Gtk.Box.BoxChild)(this.hbox10[this.label5]));
		w22.Position = 0;
		w22.Expand = false;
		w22.Fill = false;
		w22.Padding = ((uint)(4));
		// Container child hbox10.Gtk.Box+BoxChild
		this.packageName = new global::Gtk.TextView();
		this.packageName.Buffer.Text = " com.example.android.persistence";
		this.packageName.CanFocus = true;
		this.packageName.Name = "packageName";
		this.hbox10.Add(this.packageName);
		global::Gtk.Box.BoxChild w23 = ((global::Gtk.Box.BoxChild)(this.hbox10[this.packageName]));
		w23.Position = 1;
		// Container child hbox10.Gtk.Box+BoxChild
		this.kill_button = new global::Gtk.Button();
		this.kill_button.CanFocus = true;
		this.kill_button.Name = "kill_button";
		this.kill_button.UseUnderline = true;
		this.kill_button.Label = global::Mono.Unix.Catalog.GetString("Kill App");
		this.hbox10.Add(this.kill_button);
		global::Gtk.Box.BoxChild w24 = ((global::Gtk.Box.BoxChild)(this.hbox10[this.kill_button]));
		w24.Position = 2;
		w24.Expand = false;
		w24.Fill = false;
		w24.Padding = ((uint)(5));
		this.vbox3.Add(this.hbox10);
		global::Gtk.Box.BoxChild w25 = ((global::Gtk.Box.BoxChild)(this.vbox3[this.hbox10]));
		w25.Position = 5;
		w25.Expand = false;
		w25.Fill = false;
		// Container child vbox3.Gtk.Box+BoxChild
		this.hbox5 = new global::Gtk.HBox();
		this.hbox5.Name = "hbox5";
		this.hbox5.Homogeneous = true;
		this.hbox5.Spacing = 6;
		this.hbox5.BorderWidth = ((uint)(12));
		// Container child hbox5.Gtk.Box+BoxChild
		this.redButton = new global::Gtk.Button();
		this.redButton.CanFocus = true;
		this.redButton.Name = "redButton";
		this.redButton.Label = global::Mono.Unix.Catalog.GetString("RED");
		this.hbox5.Add(this.redButton);
		global::Gtk.Box.BoxChild w26 = ((global::Gtk.Box.BoxChild)(this.hbox5[this.redButton]));
		w26.Position = 0;
		// Container child hbox5.Gtk.Box+BoxChild
		this.greenButton = new global::Gtk.Button();
		this.greenButton.CanFocus = true;
		this.greenButton.Name = "greenButton";
		this.greenButton.UseUnderline = true;
		this.greenButton.Label = global::Mono.Unix.Catalog.GetString("GREEN");
		this.hbox5.Add(this.greenButton);
		global::Gtk.Box.BoxChild w27 = ((global::Gtk.Box.BoxChild)(this.hbox5[this.greenButton]));
		w27.Position = 1;
		// Container child hbox5.Gtk.Box+BoxChild
		this.yellowButton = new global::Gtk.Button();
		this.yellowButton.CanFocus = true;
		this.yellowButton.Name = "yellowButton";
		this.yellowButton.UseUnderline = true;
		this.yellowButton.Label = global::Mono.Unix.Catalog.GetString("YELLOW");
		this.hbox5.Add(this.yellowButton);
		global::Gtk.Box.BoxChild w28 = ((global::Gtk.Box.BoxChild)(this.hbox5[this.yellowButton]));
		w28.Position = 2;
		// Container child hbox5.Gtk.Box+BoxChild
		this.blueButton = new global::Gtk.Button();
		this.blueButton.CanFocus = true;
		this.blueButton.Name = "blueButton";
		this.blueButton.UseUnderline = true;
		this.blueButton.Label = global::Mono.Unix.Catalog.GetString("BLUE");
		this.hbox5.Add(this.blueButton);
		global::Gtk.Box.BoxChild w29 = ((global::Gtk.Box.BoxChild)(this.hbox5[this.blueButton]));
		w29.Position = 3;
		this.vbox3.Add(this.hbox5);
		global::Gtk.Box.BoxChild w30 = ((global::Gtk.Box.BoxChild)(this.vbox3[this.hbox5]));
		w30.Position = 6;
		w30.Expand = false;
		w30.Fill = false;
		// Container child vbox3.Gtk.Box+BoxChild
		this.hbox6 = new global::Gtk.HBox();
		this.hbox6.Name = "hbox6";
		this.hbox6.Spacing = 6;
		this.hbox6.BorderWidth = ((uint)(6));
		// Container child hbox6.Gtk.Box+BoxChild
		this.vbox11 = new global::Gtk.VBox();
		this.vbox11.Name = "vbox11";
		this.vbox11.Spacing = 6;
		// Container child vbox11.Gtk.Box+BoxChild
		this.volumeUpButton = new global::Gtk.Button();
		this.volumeUpButton.CanFocus = true;
		this.volumeUpButton.Name = "volumeUpButton";
		this.volumeUpButton.UseUnderline = true;
		this.volumeUpButton.Label = global::Mono.Unix.Catalog.GetString("VOL +");
		this.vbox11.Add(this.volumeUpButton);
		global::Gtk.Box.BoxChild w31 = ((global::Gtk.Box.BoxChild)(this.vbox11[this.volumeUpButton]));
		w31.Position = 0;
		w31.Expand = false;
		w31.Fill = false;
		// Container child vbox11.Gtk.Box+BoxChild
		this.volumeDownButton = new global::Gtk.Button();
		this.volumeDownButton.CanFocus = true;
		this.volumeDownButton.Name = "volumeDownButton";
		this.volumeDownButton.UseUnderline = true;
		this.volumeDownButton.Label = global::Mono.Unix.Catalog.GetString("VOL -");
		this.vbox11.Add(this.volumeDownButton);
		global::Gtk.Box.BoxChild w32 = ((global::Gtk.Box.BoxChild)(this.vbox11[this.volumeDownButton]));
		w32.Position = 1;
		w32.Expand = false;
		w32.Fill = false;
		// Container child vbox11.Gtk.Box+BoxChild
		this.backButton = new global::Gtk.Button();
		this.backButton.CanFocus = true;
		this.backButton.Name = "backButton";
		this.backButton.UseUnderline = true;
		this.backButton.Label = global::Mono.Unix.Catalog.GetString("BACK");
		this.vbox11.Add(this.backButton);
		global::Gtk.Box.BoxChild w33 = ((global::Gtk.Box.BoxChild)(this.vbox11[this.backButton]));
		w33.PackType = ((global::Gtk.PackType)(1));
		w33.Position = 2;
		w33.Expand = false;
		w33.Fill = false;
		this.hbox6.Add(this.vbox11);
		global::Gtk.Box.BoxChild w34 = ((global::Gtk.Box.BoxChild)(this.hbox6[this.vbox11]));
		w34.Position = 0;
		// Container child hbox6.Gtk.Box+BoxChild
		this.vbox9 = new global::Gtk.VBox();
		this.vbox9.Name = "vbox9";
		this.vbox9.Spacing = 6;
		// Container child vbox9.Gtk.Box+BoxChild
		this.channelUpButton = new global::Gtk.Button();
		this.channelUpButton.CanFocus = true;
		this.channelUpButton.Name = "channelUpButton";
		this.channelUpButton.UseUnderline = true;
		this.channelUpButton.Label = global::Mono.Unix.Catalog.GetString("CH +");
		this.vbox9.Add(this.channelUpButton);
		global::Gtk.Box.BoxChild w35 = ((global::Gtk.Box.BoxChild)(this.vbox9[this.channelUpButton]));
		w35.Position = 0;
		w35.Expand = false;
		// Container child vbox9.Gtk.Box+BoxChild
		this.channelDownButton = new global::Gtk.Button();
		this.channelDownButton.CanFocus = true;
		this.channelDownButton.Name = "channelDownButton";
		this.channelDownButton.UseUnderline = true;
		this.channelDownButton.Label = global::Mono.Unix.Catalog.GetString("CH -");
		this.vbox9.Add(this.channelDownButton);
		global::Gtk.Box.BoxChild w36 = ((global::Gtk.Box.BoxChild)(this.vbox9[this.channelDownButton]));
		w36.Position = 1;
		w36.Expand = false;
		w36.Fill = false;
		// Container child vbox9.Gtk.Box+BoxChild
		this.homeButton = new global::Gtk.Button();
		this.homeButton.CanFocus = true;
		this.homeButton.Name = "homeButton";
		this.homeButton.UseUnderline = true;
		this.homeButton.Label = global::Mono.Unix.Catalog.GetString("HOME");
		this.vbox9.Add(this.homeButton);
		global::Gtk.Box.BoxChild w37 = ((global::Gtk.Box.BoxChild)(this.vbox9[this.homeButton]));
		w37.PackType = ((global::Gtk.PackType)(1));
		w37.Position = 2;
		w37.Expand = false;
		w37.Fill = false;
		this.hbox6.Add(this.vbox9);
		global::Gtk.Box.BoxChild w38 = ((global::Gtk.Box.BoxChild)(this.hbox6[this.vbox9]));
		w38.Position = 1;
		// Container child hbox6.Gtk.Box+BoxChild
		this.leftButton = new global::Gtk.Button();
		this.leftButton.CanFocus = true;
		this.leftButton.Name = "leftButton";
		this.leftButton.UseUnderline = true;
		this.leftButton.BorderWidth = ((uint)(10));
		this.leftButton.Label = global::Mono.Unix.Catalog.GetString("◄");
		this.hbox6.Add(this.leftButton);
		global::Gtk.Box.BoxChild w39 = ((global::Gtk.Box.BoxChild)(this.hbox6[this.leftButton]));
		w39.Position = 2;
		w39.Expand = false;
		w39.Fill = false;
		// Container child hbox6.Gtk.Box+BoxChild
		this.vbox10 = new global::Gtk.VBox();
		this.vbox10.Name = "vbox10";
		this.vbox10.Homogeneous = true;
		this.vbox10.Spacing = 6;
		// Container child vbox10.Gtk.Box+BoxChild
		this.upButton = new global::Gtk.Button();
		this.upButton.CanFocus = true;
		this.upButton.Name = "upButton";
		this.upButton.UseUnderline = true;
		this.upButton.Label = global::Mono.Unix.Catalog.GetString("▲");
		this.vbox10.Add(this.upButton);
		global::Gtk.Box.BoxChild w40 = ((global::Gtk.Box.BoxChild)(this.vbox10[this.upButton]));
		w40.Position = 0;
		w40.Expand = false;
		w40.Fill = false;
		// Container child vbox10.Gtk.Box+BoxChild
		this.okButton = new global::Gtk.Button();
		this.okButton.CanFocus = true;
		this.okButton.Name = "okButton";
		this.okButton.UseUnderline = true;
		this.okButton.Label = global::Mono.Unix.Catalog.GetString("OK");
		this.vbox10.Add(this.okButton);
		global::Gtk.Box.BoxChild w41 = ((global::Gtk.Box.BoxChild)(this.vbox10[this.okButton]));
		w41.Position = 1;
		// Container child vbox10.Gtk.Box+BoxChild
		this.downButton = new global::Gtk.Button();
		this.downButton.CanFocus = true;
		this.downButton.Name = "downButton";
		this.downButton.UseUnderline = true;
		this.downButton.Label = global::Mono.Unix.Catalog.GetString("▼");
		this.vbox10.Add(this.downButton);
		global::Gtk.Box.BoxChild w42 = ((global::Gtk.Box.BoxChild)(this.vbox10[this.downButton]));
		w42.Position = 2;
		w42.Expand = false;
		w42.Fill = false;
		this.hbox6.Add(this.vbox10);
		global::Gtk.Box.BoxChild w43 = ((global::Gtk.Box.BoxChild)(this.hbox6[this.vbox10]));
		w43.Position = 3;
		w43.Expand = false;
		w43.Fill = false;
		// Container child hbox6.Gtk.Box+BoxChild
		this.rightButton = new global::Gtk.Button();
		this.rightButton.CanFocus = true;
		this.rightButton.Name = "rightButton";
		this.rightButton.UseUnderline = true;
		this.rightButton.BorderWidth = ((uint)(10));
		this.rightButton.Label = global::Mono.Unix.Catalog.GetString("►");
		this.hbox6.Add(this.rightButton);
		global::Gtk.Box.BoxChild w44 = ((global::Gtk.Box.BoxChild)(this.hbox6[this.rightButton]));
		w44.Position = 4;
		w44.Expand = false;
		w44.Fill = false;
		this.vbox3.Add(this.hbox6);
		global::Gtk.Box.BoxChild w45 = ((global::Gtk.Box.BoxChild)(this.vbox3[this.hbox6]));
		w45.Position = 7;
		w45.Expand = false;
		w45.Fill = false;
		// Container child vbox3.Gtk.Box+BoxChild
		this.hbox7 = new global::Gtk.HBox();
		this.hbox7.Name = "hbox7";
		this.hbox7.Homogeneous = true;
		// Container child hbox7.Gtk.Box+BoxChild
		this.rewindButton = new global::Gtk.Button();
		this.rewindButton.CanFocus = true;
		this.rewindButton.Name = "rewindButton";
		this.rewindButton.UseUnderline = true;
		this.rewindButton.Label = global::Mono.Unix.Catalog.GetString("◄◄");
		this.hbox7.Add(this.rewindButton);
		global::Gtk.Box.BoxChild w46 = ((global::Gtk.Box.BoxChild)(this.hbox7[this.rewindButton]));
		w46.Position = 0;
		// Container child hbox7.Gtk.Box+BoxChild
		this.playButton = new global::Gtk.Button();
		this.playButton.CanFocus = true;
		this.playButton.Name = "playButton";
		this.playButton.UseUnderline = true;
		this.playButton.Label = global::Mono.Unix.Catalog.GetString("PLAY");
		this.hbox7.Add(this.playButton);
		global::Gtk.Box.BoxChild w47 = ((global::Gtk.Box.BoxChild)(this.hbox7[this.playButton]));
		w47.Position = 1;
		// Container child hbox7.Gtk.Box+BoxChild
		this.fastforwardButton = new global::Gtk.Button();
		this.fastforwardButton.CanFocus = true;
		this.fastforwardButton.Name = "fastforwardButton";
		this.fastforwardButton.UseUnderline = true;
		this.fastforwardButton.Label = global::Mono.Unix.Catalog.GetString("►►");
		this.hbox7.Add(this.fastforwardButton);
		global::Gtk.Box.BoxChild w48 = ((global::Gtk.Box.BoxChild)(this.hbox7[this.fastforwardButton]));
		w48.Position = 2;
		this.vbox3.Add(this.hbox7);
		global::Gtk.Box.BoxChild w49 = ((global::Gtk.Box.BoxChild)(this.vbox3[this.hbox7]));
		w49.Position = 8;
		w49.Expand = false;
		w49.Fill = false;
		// Container child vbox3.Gtk.Box+BoxChild
		this.hbox8 = new global::Gtk.HBox();
		this.hbox8.Name = "hbox8";
		this.hbox8.Homogeneous = true;
		this.hbox8.Spacing = 6;
		// Container child hbox8.Gtk.Box+BoxChild
		this.skipPrevButton = new global::Gtk.Button();
		this.skipPrevButton.CanFocus = true;
		this.skipPrevButton.Name = "skipPrevButton";
		this.skipPrevButton.UseUnderline = true;
		this.skipPrevButton.Label = global::Mono.Unix.Catalog.GetString("|◄◄");
		this.hbox8.Add(this.skipPrevButton);
		global::Gtk.Box.BoxChild w50 = ((global::Gtk.Box.BoxChild)(this.hbox8[this.skipPrevButton]));
		w50.Position = 0;
		// Container child hbox8.Gtk.Box+BoxChild
		this.pauseButton = new global::Gtk.Button();
		this.pauseButton.CanFocus = true;
		this.pauseButton.Name = "pauseButton";
		this.pauseButton.UseUnderline = true;
		this.pauseButton.Label = global::Mono.Unix.Catalog.GetString("PAUSE");
		this.hbox8.Add(this.pauseButton);
		global::Gtk.Box.BoxChild w51 = ((global::Gtk.Box.BoxChild)(this.hbox8[this.pauseButton]));
		w51.Position = 1;
		// Container child hbox8.Gtk.Box+BoxChild
		this.skipNextButton = new global::Gtk.Button();
		this.skipNextButton.CanFocus = true;
		this.skipNextButton.Name = "skipNextButton";
		this.skipNextButton.UseUnderline = true;
		this.skipNextButton.Label = global::Mono.Unix.Catalog.GetString("►►|");
		this.hbox8.Add(this.skipNextButton);
		global::Gtk.Box.BoxChild w52 = ((global::Gtk.Box.BoxChild)(this.hbox8[this.skipNextButton]));
		w52.Position = 2;
		this.vbox3.Add(this.hbox8);
		global::Gtk.Box.BoxChild w53 = ((global::Gtk.Box.BoxChild)(this.vbox3[this.hbox8]));
		w53.Position = 9;
		w53.Expand = false;
		w53.Fill = false;
		// Container child vbox3.Gtk.Box+BoxChild
		this.stopButton = new global::Gtk.Button();
		this.stopButton.CanFocus = true;
		this.stopButton.Name = "stopButton";
		this.stopButton.UseUnderline = true;
		this.stopButton.Label = global::Mono.Unix.Catalog.GetString("STOP");
		this.vbox3.Add(this.stopButton);
		global::Gtk.Box.BoxChild w54 = ((global::Gtk.Box.BoxChild)(this.vbox3[this.stopButton]));
		w54.Position = 10;
		w54.Expand = false;
		w54.Fill = false;
		// Container child vbox3.Gtk.Box+BoxChild
		this.hbox9 = new global::Gtk.HBox();
		this.hbox9.Name = "hbox9";
		this.hbox9.Spacing = 6;
		this.hbox9.BorderWidth = ((uint)(6));
		// Container child hbox9.Gtk.Box+BoxChild
		this.label4 = new global::Gtk.Label();
		this.label4.Name = "label4";
		this.label4.LabelProp = global::Mono.Unix.Catalog.GetString("Custom ADB Command:");
		this.hbox9.Add(this.label4);
		global::Gtk.Box.BoxChild w55 = ((global::Gtk.Box.BoxChild)(this.hbox9[this.label4]));
		w55.Position = 0;
		w55.Expand = false;
		w55.Fill = false;
		// Container child hbox9.Gtk.Box+BoxChild
		this.customCommandText = new global::Gtk.TextView();
		this.customCommandText.CanFocus = true;
		this.customCommandText.Name = "customCommandText";
		this.hbox9.Add(this.customCommandText);
		global::Gtk.Box.BoxChild w56 = ((global::Gtk.Box.BoxChild)(this.hbox9[this.customCommandText]));
		w56.Position = 1;
		// Container child hbox9.Gtk.Box+BoxChild
		this.executeButton = new global::Gtk.Button();
		this.executeButton.CanFocus = true;
		this.executeButton.Name = "executeButton";
		this.executeButton.UseUnderline = true;
		this.executeButton.Label = global::Mono.Unix.Catalog.GetString("EXECUTE");
		this.hbox9.Add(this.executeButton);
		global::Gtk.Box.BoxChild w57 = ((global::Gtk.Box.BoxChild)(this.hbox9[this.executeButton]));
		w57.Position = 2;
		w57.Expand = false;
		w57.Fill = false;
		this.vbox3.Add(this.hbox9);
		global::Gtk.Box.BoxChild w58 = ((global::Gtk.Box.BoxChild)(this.vbox3[this.hbox9]));
		w58.Position = 11;
		w58.Expand = false;
		w58.Fill = false;
		// Container child vbox3.Gtk.Box+BoxChild
		this.label3 = new global::Gtk.Label();
		this.label3.Name = "label3";
		this.label3.Xalign = 0F;
		this.label3.LabelProp = global::Mono.Unix.Catalog.GetString("Log");
		this.vbox3.Add(this.label3);
		global::Gtk.Box.BoxChild w59 = ((global::Gtk.Box.BoxChild)(this.vbox3[this.label3]));
		w59.Position = 12;
		w59.Expand = false;
		w59.Fill = false;
		// Container child vbox3.Gtk.Box+BoxChild
		this.GtkScrolledWindow = new global::Gtk.ScrolledWindow();
		this.GtkScrolledWindow.HeightRequest = 100;
		this.GtkScrolledWindow.Name = "GtkScrolledWindow";
		this.GtkScrolledWindow.ShadowType = ((global::Gtk.ShadowType)(1));
		this.GtkScrolledWindow.BorderWidth = ((uint)(3));
		// Container child GtkScrolledWindow.Gtk.Container+ContainerChild
		this.logText = new global::Gtk.TextView();
		this.logText.HeightRequest = 100;
		this.logText.CanFocus = true;
		this.logText.Name = "logText";
		this.logText.Editable = false;
		this.logText.AcceptsTab = false;
		this.logText.WrapMode = ((global::Gtk.WrapMode)(1));
		this.logText.LeftMargin = 4;
		this.logText.RightMargin = 4;
		this.GtkScrolledWindow.Add(this.logText);
		this.vbox3.Add(this.GtkScrolledWindow);
		global::Gtk.Box.BoxChild w61 = ((global::Gtk.Box.BoxChild)(this.vbox3[this.GtkScrolledWindow]));
		w61.Position = 13;
		w61.Padding = ((uint)(4));
		this.Add(this.vbox3);
		if ((this.Child != null))
		{
			this.Child.ShowAll();
		}
		this.DefaultWidth = 600;
		this.DefaultHeight = 741;
		this.Show();
		this.DeleteEvent += new global::Gtk.DeleteEventHandler(this.OnDeleteEvent);
		this.startServerButton.Clicked += new global::System.EventHandler(this.OnStartServerButtonClicked);
		this.killServerButton.Clicked += new global::System.EventHandler(this.OnKillServerButtonClicked);
		this.devicesButton.Clicked += new global::System.EventHandler(this.OnDevicesButtonClicked);
		this.rebootButton.Clicked += new global::System.EventHandler(this.OnRebootButtonClicked);
		this.screencapButton.Clicked += new global::System.EventHandler(this.OnScreencapButtonClicked);
		this.browseButton.Clicked += new global::System.EventHandler(this.OnBrowseButtonClicked);
		this.installButton.Clicked += new global::System.EventHandler(this.OnInstallButtonClicked);
		this.connectButton.Clicked += new global::System.EventHandler(this.OnConnectButtonClicked);
		this.kill_button.Clicked += new global::System.EventHandler(this.OnKillButtonClicked);
		this.redButton.Clicked += new global::System.EventHandler(this.OnRedButtonClicked);
		this.greenButton.Clicked += new global::System.EventHandler(this.OnGreenButtonClicked);
		this.yellowButton.Clicked += new global::System.EventHandler(this.OnYellowButtonClicked);
		this.blueButton.Clicked += new global::System.EventHandler(this.OnBlueButtonClicked);
		this.volumeUpButton.Clicked += new global::System.EventHandler(this.OnVolumeUpButtonClicked);
		this.volumeDownButton.Clicked += new global::System.EventHandler(this.OnVolumeDownButtonClicked);
		this.backButton.Clicked += new global::System.EventHandler(this.OnBackButtonClicked);
		this.channelUpButton.Clicked += new global::System.EventHandler(this.OnChannelUpButtonClicked);
		this.channelDownButton.Clicked += new global::System.EventHandler(this.OnChannelDownButtonClicked);
		this.homeButton.Clicked += new global::System.EventHandler(this.OnHomeButtonClicked);
		this.leftButton.Clicked += new global::System.EventHandler(this.OnLeftButtonClicked);
		this.upButton.Clicked += new global::System.EventHandler(this.OnUpButtonClicked);
		this.okButton.Clicked += new global::System.EventHandler(this.OnOkButtonClicked);
		this.downButton.Clicked += new global::System.EventHandler(this.OnDownButtonClicked);
		this.rightButton.Clicked += new global::System.EventHandler(this.OnRightButtonClicked);
		this.rewindButton.Clicked += new global::System.EventHandler(this.OnRewindButtonClicked);
		this.playButton.Clicked += new global::System.EventHandler(this.OnPlayButtonClicked);
		this.fastforwardButton.Clicked += new global::System.EventHandler(this.OnFastforwardButtonClicked);
		this.skipPrevButton.Clicked += new global::System.EventHandler(this.OnSkipPrevButtonClicked);
		this.pauseButton.Clicked += new global::System.EventHandler(this.OnPauseButtonClicked);
		this.skipNextButton.Clicked += new global::System.EventHandler(this.OnSkipNextButtonClicked);
		this.stopButton.Clicked += new global::System.EventHandler(this.OnStopButtonClicked);
		this.executeButton.Clicked += new global::System.EventHandler(this.OnExecuteButtonClicked);
	}
}
