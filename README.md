# ADB Toolkit for Android TV


Supports basic ADB commands & also most of Android TV remote control keys.
Tested and works on Ubuntu 16.04 LTS.  

## How to Use
1. Download this repository.

	```git clone https://mankidal19@bitbucket.org/mankidal19/adbtoolkit_fortv.git```
	   
2. Run the **ADBToolKit.exe** file in ADBToolKit/bin/Debug.

### Beta Version 0.1 GUI
![Beta Version 0.1 GUI](https://i.postimg.cc/Dzb6C4rV/adbtoolkit.png "Beta Version 0.1 GUI")  

### Beta Version 0.2 GUI
![Beta Version 0.2 GUI](https://i.postimg.cc/DZ0ZYtqR/adb-Tool-Kitv2.png "Beta Version 0.2 GUI")  
* Now supports screencap feature!